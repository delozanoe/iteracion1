CREATE TABLE GIMNASIO 
(
  ID INTEGER NOT NULL 
, IDHOTEL INTEGER NOT NULL 
, CAPACIDAD INTEGER NOT NULL 
, MAQUINAS VARCHAR2(500) NOT NULL 
, HORARIOAPERTURA DATE NOT NULL 
, HORARIOCIERRE DATE NOT NULL 
, COSTOINCLUIDO VARCHAR(1) NOT NULL 
, COSTOADICIONAL NUMBER NOT NULL 
, CONSTRAINT GIMNASIO_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);
