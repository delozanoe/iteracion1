
    create table Bar (
        id bigint not null,
        capacidad integer not null,
        estilo varchar(255) not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Bar_Producto (
        Bar_id bigint not null,
        consumos_id bigint not null,
        primary key (Bar_id, consumos_id)
    );

    create table ConsumoPorHabitacion (
        id bigint not null,
        valorTotal double not null,
        primary key (id)
    );

    create table ConsumoPorHabitacion_Bar (
        ConsumoPorHabitacion_id bigint not null,
        bares_id bigint not null,
        primary key (ConsumoPorHabitacion_id, bares_id)
    );

    create table ConsumoPorHabitacion_Gimnasio (
        ConsumoPorHabitacion_id bigint not null,
        gimnasio_id bigint not null,
        primary key (ConsumoPorHabitacion_id, gimnasio_id)
    );

    create table ConsumoPorHabitacion_Internet (
        ConsumoPorHabitacion_id bigint not null,
        internet_id bigint not null,
        primary key (ConsumoPorHabitacion_id, internet_id)
    );

    create table ConsumoPorHabitacion_Piscina (
        ConsumoPorHabitacion_id bigint not null,
        piscina_id bigint not null,
        primary key (ConsumoPorHabitacion_id, piscina_id)
    );

    create table ConsumoPorHabitacion_Prestamo (
        ConsumoPorHabitacion_id bigint not null,
        prestamos_id bigint not null,
        primary key (ConsumoPorHabitacion_id, prestamos_id),
        unique (prestamos_id)
    );

    create table ConsumoPorHabitacion_Producto (
        ConsumoPorHabitacion_id bigint not null,
        productoHabitacion_id bigint not null,
        primary key (ConsumoPorHabitacion_id, productoHabitacion_id),
        unique (productoHabitacion_id)
    );

    create table ConsumoPorHabitacion_Salon (
        ConsumoPorHabitacion_id bigint not null,
        salones_id bigint not null,
        primary key (ConsumoPorHabitacion_id, salones_id)
    );

    create table ConsumoPorHabitacion_ServicioPrendas (
        ConsumoPorHabitacion_id bigint not null,
        servicioPrendas_id bigint not null,
        primary key (ConsumoPorHabitacion_id, servicioPrendas_id)
    );

    create table ConsumoPorHabitacion_Spa (
        ConsumoPorHabitacion_id bigint not null,
        spa_id bigint not null,
        primary key (ConsumoPorHabitacion_id, spa_id)
    );

    create table ConsumoPorHabitacion_Supermercado (
        ConsumoPorHabitacion_id bigint not null,
        supermercado_id bigint not null,
        primary key (ConsumoPorHabitacion_id, supermercado_id)
    );

    create table ConsumoPorHabitacion_Tienda (
        ConsumoPorHabitacion_id bigint not null,
        tiendas_id bigint not null
    );

    create table FormaDePago (
        id bigint not null,
        nombre varchar(255) not null,
        primary key (id)
    );

    create table Gimnasio (
        id bigint not null,
        capacidad integer not null,
        costoAdicional double not null,
        costoIncluido bit not null,
        horarioApertura date,
        horarioCierre date,
        maquinas varchar(255) not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Habitacion (
        id bigint not null,
        capacidad integer not null,
        costoPorNoche double not null,
        cuenta double not null,
        tipoDeHabitacion varchar(255) not null,
        consumoHabitacion_id bigint,
        hotel_id bigint not null,
        planConsumo_id bigint,
        primary key (id)
    );

    create table Hotel (
        id bigint not null,
        ciudad varchar(255) not null,
        direccion varchar(255) not null,
        ofertaHabitacional integer not null,
        pais varchar(255) not null,
        administradorDatos_id bigint,
        gerente_id bigint,
        primary key (id)
    );

    create table Internet (
        id bigint not null,
        capacidad double not null,
        costoIncluido bit not null,
        costoPorDia double not null,
        hotel_id bigint,
        primary key (id)
    );

    create table Piscina (
        id bigint not null,
        capacidad integer not null,
        costoAdicional double not null,
        costoIncluido bit not null,
        horaApertura date,
        horaCierre date,
        profundidad double not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table PlanConsumo (
        id bigint not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Prestamo (
        id bigint not null,
        costo double not null,
        devuelto bit not null,
        utensilio varchar(255) not null,
        habitacion_id bigint not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Producto (
        id bigint not null,
        cargadoAHabitacion bit not null,
        costo double not null,
        nombre varchar(255) not null,
        supermercado_id bigint,
        primary key (id)
    );

    create table Producto_Bar (
        cartaProductos_id bigint not null,
        bar_id bigint not null,
        primary key (cartaProductos_id, bar_id)
    );

    create table Reserva (
        id bigint not null,
        dia date,
        duracion integer not null,
        horaInicio date,
        lugar varchar(255) not null,
        cliente_id bigint not null,
        servicio_id bigint not null,
        primary key (id)
    );

    create table ReservaHabitacion (
        id bigint not null,
        fechaEntrada date,
        fechaSalida date,
        numeroPersonas integer not null,
        cliente_id bigint not null,
        formaDePago_id bigint,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Restaurante (
        id bigint not null,
        capacidad integer not null,
        estilo varchar(255) not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Restaurante_Producto (
        Restaurante_id bigint not null,
        consumos_id bigint not null,
        restaurantes_id bigint not null,
        cartaProductos_id bigint not null,
        primary key (restaurantes_id, cartaProductos_id)
    );

    create table Salon (
        id bigint not null,
        capacidad integer not null,
        costoAdicional double not null,
        costoPorHora double not null,
        tipo varchar(255) not null,
        usoEquipos bit not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Salon_Reserva (
        salones_id bigint not null,
        reservas_id bigint not null,
        primary key (salones_id, reservas_id)
    );

    create table ServicioPrendas (
        id bigint not null,
        costoTotal double not null,
        numeroPrendas integer not null,
        tipo varchar(255) not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table ServicioSpa (
        id bigint not null,
        cargadoAHabitacion bit not null,
        costo double not null,
        duracion integer not null,
        nombre varchar(255) not null,
        spa_id bigint not null,
        primary key (id)
    );

    create table Spa (
        id bigint not null,
        capacidad integer not null,
        hotel_id bigint,
        primary key (id)
    );

    create table Spa_Reserva (
        Spa_id bigint not null,
        reservas_id bigint not null,
        primary key (Spa_id, reservas_id),
        unique (reservas_id)
    );

    create table Spa_ServicioSpa (
        Spa_id bigint not null,
        consumos_id bigint not null,
        primary key (Spa_id, consumos_id),
        unique (consumos_id)
    );

    create table Supermercado (
        id bigint not null,
        hotel_id bigint,
        primary key (id)
    );

    create table Supermercado_Producto (
        Supermercado_id bigint not null,
        consumos_id bigint not null,
        primary key (Supermercado_id, consumos_id),
        unique (consumos_id)
    );

    create table Tienda (
        id bigint not null,
        nombre varchar(255) not null,
        tipo varchar(255) not null,
        hotel_id bigint not null,
        primary key (id)
    );

    create table Tienda_Producto (
        tiendas_id bigint not null,
        productos_id bigint not null,
        Tienda_id bigint not null,
        consumos_id bigint not null,
        primary key (Tienda_id, consumos_id),
        unique (consumos_id)
    );

    create table Usuario (
        DTYPE varchar(31) not null,
        id bigint not null,
        correo varchar(255) not null,
        nombre varchar(255) not null,
        numeroDocumento bigint not null,
        tipoDocumento varchar(255) not null,
        pazYSalvo bit not null,
        hotel_id bigint not null,
        habitacion_id bigint not null,
        primary key (id)
    );

    alter table Bar 
        add constraint FK103F3F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Bar_Producto 
        add constraint FKFC94244CACC81CBA 
        foreign key (Bar_id) 
        references Bar;

    alter table Bar_Producto 
        add constraint FKFC94244C956976ED 
        foreign key (consumos_id) 
        references Producto;

    alter table ConsumoPorHabitacion_Bar 
        add constraint FKB214D7D397BE1A0C 
        foreign key (bares_id) 
        references Bar;

    alter table ConsumoPorHabitacion_Bar 
        add constraint FKB214D7D336FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Gimnasio 
        add constraint FK70B3819BF59B13FA 
        foreign key (gimnasio_id) 
        references Gimnasio;

    alter table ConsumoPorHabitacion_Gimnasio 
        add constraint FK70B3819B36FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Internet 
        add constraint FK546FC38136FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Internet 
        add constraint FK546FC381AA10023A 
        foreign key (internet_id) 
        references Internet;

    alter table ConsumoPorHabitacion_Piscina 
        add constraint FK5BE968F336FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Piscina 
        add constraint FK5BE968F3CA6E18BA 
        foreign key (piscina_id) 
        references Piscina;

    alter table ConsumoPorHabitacion_Prestamo 
        add constraint FKE65E3B3F3CA70305 
        foreign key (prestamos_id) 
        references Prestamo;

    alter table ConsumoPorHabitacion_Prestamo 
        add constraint FKE65E3B3F36FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Producto 
        add constraint FKF69BCDA0E19174E8 
        foreign key (productoHabitacion_id) 
        references Producto;

    alter table ConsumoPorHabitacion_Producto 
        add constraint FKF69BCDA036FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Salon 
        add constraint FK812DB5FD73F7EC4C 
        foreign key (salones_id) 
        references Salon;

    alter table ConsumoPorHabitacion_Salon 
        add constraint FK812DB5FD36FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_ServicioPrendas 
        add constraint FK9499F39536FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_ServicioPrendas 
        add constraint FK9499F39599E3C15A 
        foreign key (servicioPrendas_id) 
        references ServicioPrendas;

    alter table ConsumoPorHabitacion_Spa 
        add constraint FKB2151964CA96671A 
        foreign key (spa_id) 
        references Spa;

    alter table ConsumoPorHabitacion_Spa 
        add constraint FKB215196436FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Supermercado 
        add constraint FKA127BEA836FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Supermercado 
        add constraint FKA127BEA89258CCDA 
        foreign key (supermercado_id) 
        references Supermercado;

    alter table ConsumoPorHabitacion_Tienda 
        add constraint FKA6AB6B1B36FBBD7A 
        foreign key (ConsumoPorHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table ConsumoPorHabitacion_Tienda 
        add constraint FKA6AB6B1B7D9C7CBD 
        foreign key (tiendas_id) 
        references Tienda;

    alter table Gimnasio 
        add constraint FK421DE97BF32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Habitacion 
        add constraint FK24AF03D24E119C5A 
        foreign key (planConsumo_id) 
        references PlanConsumo;

    alter table Habitacion 
        add constraint FK24AF03D291BEDEA1 
        foreign key (consumoHabitacion_id) 
        references ConsumoPorHabitacion;

    alter table Habitacion 
        add constraint FK24AF03D2F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Hotel 
        add constraint FK42AD194A98B2DA 
        foreign key (gerente_id) 
        references Usuario;

    alter table Hotel 
        add constraint FK42AD1945E0D80FA 
        foreign key (administradorDatos_id) 
        references Usuario;

    alter table Internet 
        add constraint FK25DA2B61F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Piscina 
        add constraint FK41A28513F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table PlanConsumo 
        add constraint FK312807DF32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Prestamo 
        add constraint FKB7C8A31FDFBED89A 
        foreign key (habitacion_id) 
        references Habitacion;

    alter table Prestamo 
        add constraint FKB7C8A31FF32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Producto 
        add constraint FKC80635809258CCDA 
        foreign key (supermercado_id) 
        references Supermercado;

    alter table Producto_Bar 
        add constraint FK471BA8B4ACC81CBA 
        foreign key (bar_id) 
        references Bar;

    alter table Producto_Bar 
        add constraint FK471BA8B46B301F08 
        foreign key (cartaProductos_id) 
        references Producto;

    alter table Reserva 
        add constraint FKA49CA4987E10CDF2 
        foreign key (servicio_id) 
        references ServicioSpa;

    alter table Reserva 
        add constraint FKA49CA498D8418F5A 
        foreign key (cliente_id) 
        references Usuario;

    alter table ReservaHabitacion 
        add constraint FKA098CA6AF32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table ReservaHabitacion 
        add constraint FKA098CA6AEAE4C27A 
        foreign key (formaDePago_id) 
        references FormaDePago;

    alter table ReservaHabitacion 
        add constraint FKA098CA6AD8418F5A 
        foreign key (cliente_id) 
        references Usuario;

    alter table Restaurante 
        add constraint FK9858C4A8F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Restaurante_Producto 
        add constraint FKA2E15F376B301F08 
        foreign key (cartaProductos_id) 
        references Producto;

    alter table Restaurante_Producto 
        add constraint FKA2E15F379620849A 
        foreign key (Restaurante_id) 
        references Restaurante;

    alter table Restaurante_Producto 
        add constraint FKA2E15F375391F5D7 
        foreign key (restaurantes_id) 
        references Restaurante;

    alter table Restaurante_Producto 
        add constraint FKA2E15F37956976ED 
        foreign key (consumos_id) 
        references Producto;

    alter table Salon 
        add constraint FK4BF5A1DF32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Salon_Reserva 
        add constraint FKE1B2387673F7EC4C 
        foreign key (salones_id) 
        references Salon;

    alter table Salon_Reserva 
        add constraint FKE1B2387647714DB7 
        foreign key (reservas_id) 
        references Reserva;

    alter table ServicioPrendas 
        add constraint FKB604EFB5F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table ServicioSpa 
        add constraint FKF0BD24CECA96671A 
        foreign key (spa_id) 
        references Spa;

    alter table Spa 
        add constraint FK14584F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Spa_Reserva 
        add constraint FK8561CCDDCA96671A 
        foreign key (Spa_id) 
        references Spa;

    alter table Spa_Reserva 
        add constraint FK8561CCDD47714DB7 
        foreign key (reservas_id) 
        references Reserva;

    alter table Spa_ServicioSpa 
        add constraint FK1E7AA293CA96671A 
        foreign key (Spa_id) 
        references Spa;

    alter table Spa_ServicioSpa 
        add constraint FK1E7AA293BE20663B 
        foreign key (consumos_id) 
        references ServicioSpa;

    alter table Supermercado 
        add constraint FK655B3688F32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Supermercado_Producto 
        add constraint FK83510957956976ED 
        foreign key (consumos_id) 
        references Producto;

    alter table Supermercado_Producto 
        add constraint FK835109579258CCDA 
        foreign key (Supermercado_id) 
        references Supermercado;

    alter table Tienda 
        add constraint FK954E4AFBF32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Tienda_Producto 
        add constraint FKE924644FFE52BFA 
        foreign key (Tienda_id) 
        references Tienda;

    alter table Tienda_Producto 
        add constraint FKE9246447D9C7CBD 
        foreign key (tiendas_id) 
        references Tienda;

    alter table Tienda_Producto 
        add constraint FKE9246445600F47 
        foreign key (productos_id) 
        references Producto;

    alter table Tienda_Producto 
        add constraint FKE924644956976ED 
        foreign key (consumos_id) 
        references Producto;

    alter table Usuario 
        add constraint FK5B4D8B0EF32CB51A 
        foreign key (hotel_id) 
        references Hotel;

    alter table Usuario 
        add constraint FK5B4D8B0EDFBED89A 
        foreign key (habitacion_id) 
        references Habitacion;
