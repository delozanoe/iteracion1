package uniandes.hotelAndes.negocio.asociaciones;

public class TiendaVendioProducto 
{
	private Integer idTienda; 
	
	private Integer idProducto;

	public TiendaVendioProducto(Integer idTienda, Integer idProducto) {
		super();
		this.idTienda = idTienda;
		this.idProducto = idProducto;
	}

	public Integer getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(Integer idTienda) {
		this.idTienda = idTienda;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	} 
	
	
}
