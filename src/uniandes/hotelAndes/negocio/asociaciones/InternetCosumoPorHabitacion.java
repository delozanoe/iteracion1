package uniandes.hotelAndes.negocio.asociaciones;

public class InternetCosumoPorHabitacion 
{
	private Integer idInternet;
	
	private Integer idConsumoPorHabitacion;

	public InternetCosumoPorHabitacion(Integer idInternet, Integer idConsumoPorHabitacion) {
		super();
		this.idInternet = idInternet;
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}

	public Integer getIdInternet() {
		return idInternet;
	}

	public void setIdInternet(Integer idInternet) {
		this.idInternet = idInternet;
	}

	public Integer getIdConsumoPorHabitacion() {
		return idConsumoPorHabitacion;
	}

	public void setIdConsumoPorHabitacion(Integer idConsumoPorHabitacion) {
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}
	
	
}
