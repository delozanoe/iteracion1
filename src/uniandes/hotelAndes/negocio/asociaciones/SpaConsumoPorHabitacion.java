package uniandes.hotelAndes.negocio.asociaciones;

public class SpaConsumoPorHabitacion 
{
	private Integer idSpa;
	
	private Integer idConsumoPorHabitacion;

	public SpaConsumoPorHabitacion(Integer idSpa, Integer idConsumoPorHabitacion) {
		super();
		this.idSpa = idSpa;
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}

	public Integer getIdSpa() {
		return idSpa;
	}

	public void setIdSpa(Integer idSpa) {
		this.idSpa = idSpa;
	}

	public Integer getIdConsumoPorHabitacion() {
		return idConsumoPorHabitacion;
	}

	public void setIdConsumoPorHabitacion(Integer idConsumoPorHabitacion) {
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}
	
	
}
