package uniandes.hotelAndes.negocio.asociaciones;

public class PrestamoConsumoPorHabitacion 
{
	private Integer idPrestamo;
	
	private Integer idConsumoPorHabitacion;

	public PrestamoConsumoPorHabitacion(Integer idPrestamo, Integer idConsumoPorHabitacion) {
		super();
		this.idPrestamo = idPrestamo;
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}

	public Integer getIdPrestamo() {
		return idPrestamo;
	}

	public void setIdPrestamo(Integer idPrestamo) {
		this.idPrestamo = idPrestamo;
	}

	public Integer getIdConsumoPorHabitacion() {
		return idConsumoPorHabitacion;
	}

	public void setIdConsumoPorHabitacion(Integer idConsumoPorHabitacion) {
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}
	
	
}
