package uniandes.hotelAndes.negocio.asociaciones;

public class SupermercadoConsumoPorHabitacion 
{
	private Integer idSupermercado;
	
	private Integer idCosumoPorHabitacion;

	public SupermercadoConsumoPorHabitacion(Integer idSupermercado, Integer idCosumoPorHabitacion) {
		super();
		this.idSupermercado = idSupermercado;
		this.idCosumoPorHabitacion = idCosumoPorHabitacion;
	}

	public Integer getIdSupermercado() {
		return idSupermercado;
	}

	public void setIdSupermercado(Integer idSupermercado) {
		this.idSupermercado = idSupermercado;
	}

	public Integer getIdCosumoPorHabitacion() {
		return idCosumoPorHabitacion;
	}

	public void setIdCosumoPorHabitacion(Integer idCosumoPorHabitacion) {
		this.idCosumoPorHabitacion = idCosumoPorHabitacion;
	}

	
}
