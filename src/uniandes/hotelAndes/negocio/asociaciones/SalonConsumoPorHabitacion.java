package uniandes.hotelAndes.negocio.asociaciones;

public class SalonConsumoPorHabitacion 
{
	private Integer idSalon; 
	
	private Integer idConsumoPorHabitacion;

	public SalonConsumoPorHabitacion(Integer idSalon, Integer idConsumoPorHabitacion) {
		super();
		this.idSalon = idSalon;
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}

	public Integer getIdSalon() {
		return idSalon;
	}

	public void setIdSalon(Integer idSalon) {
		this.idSalon = idSalon;
	}

	public Integer getIdConsumoPorHabitacion() {
		return idConsumoPorHabitacion;
	}

	public void setIdConsumoPorHabitacion(Integer idConsumoPorHabitacion) {
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}
	
	
}
