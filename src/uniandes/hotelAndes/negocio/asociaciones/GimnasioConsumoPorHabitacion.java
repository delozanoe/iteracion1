package uniandes.hotelAndes.negocio.asociaciones;

public class GimnasioConsumoPorHabitacion 
{
	private Integer idGimnasio; 
	
	private Integer idConsumoPorHabitacion;

	public GimnasioConsumoPorHabitacion(Integer idGimnasio, Integer idConsumoPorHabitacion) {
		super();
		this.idGimnasio = idGimnasio;
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}

	public Integer getIdGimnasio() {
		return idGimnasio;
	}

	public void setIdGimnasio(Integer idGimnasio) {
		this.idGimnasio = idGimnasio;
	}

	public Integer getIdConsumoPorHabitacion() {
		return idConsumoPorHabitacion;
	}

	public void setIdConsumoPorHabitacion(Integer idConsumoPorHabitacion) {
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}
	
	
}
