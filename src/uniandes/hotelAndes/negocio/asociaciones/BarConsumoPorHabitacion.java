package uniandes.hotelAndes.negocio.asociaciones;

public class BarConsumoPorHabitacion 
{
	private Integer idBar;
	
	private Integer idCosnumoPorHabitacion;

	public BarConsumoPorHabitacion(Integer idBar, Integer idCosnumoPorHabitacion) {
		super();
		this.idBar = idBar;
		this.idCosnumoPorHabitacion = idCosnumoPorHabitacion;
	}

	public Integer getIdBar() {
		return idBar;
	}

	public void setIdBar(Integer idBar) {
		this.idBar = idBar;
	}

	public Integer getIdCosnumoPorHabitacion() {
		return idCosnumoPorHabitacion;
	}

	public void setIdCosnumoPorHabitacion(Integer idCosnumoPorHabitacion) {
		this.idCosnumoPorHabitacion = idCosnumoPorHabitacion;
	}
	
	
}
