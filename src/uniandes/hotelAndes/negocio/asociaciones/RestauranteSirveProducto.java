package uniandes.hotelAndes.negocio.asociaciones;

public class RestauranteSirveProducto 
{
	private Integer idRestaurante;
	
	private Integer idProducto;

	public RestauranteSirveProducto(Integer idRestaurante, Integer idProducto) {
		super();
		this.idRestaurante = idRestaurante;
		this.idProducto = idProducto;
	}

	public Integer getIdRestaurante() {
		return idRestaurante;
	}

	public void setIdRestaurante(Integer idRestaurante) {
		this.idRestaurante = idRestaurante;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	
	
}
