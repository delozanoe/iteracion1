package uniandes.hotelAndes.negocio.asociaciones;

public class TiendaConsumoPorHabitacion 
{
	private Integer idTienda;
	
	private Integer idConsumoPorHabitacion;

	public TiendaConsumoPorHabitacion(Integer idTienda, Integer idConsumoPorHabitacion) {
		super();
		this.idTienda = idTienda;
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}

	public Integer getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(Integer idTienda) {
		this.idTienda = idTienda;
	}

	public Integer getIdConsumoPorHabitacion() {
		return idConsumoPorHabitacion;
	}

	public void setIdConsumoPorHabitacion(Integer idConsumoPorHabitacion) {
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}
	
	
}
