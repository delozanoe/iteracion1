package uniandes.hotelAndes.negocio.asociaciones;

public class SalonReserva 
{
	private Integer idSalon; 
	
	private Integer idReserva;
	
	public SalonReserva(Integer idSalon, Integer idReserva )
	{
		this.idSalon = idSalon;
				this.idReserva = idReserva; 
	}

	public Integer getIdSalon() {
		return idSalon;
	}

	public void setIdSalon(Integer idSalon) {
		this.idSalon = idSalon;
	}

	public Integer getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(Integer idReserva) {
		this.idReserva = idReserva;
	}
	
	
	
}
