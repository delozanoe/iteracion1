package uniandes.hotelAndes.negocio.asociaciones;

public class BarSirveProducto
{
	private Integer idBar; 
	
	private Integer idProducto;

	public BarSirveProducto(Integer idBar, Integer idProducto) {
		super();
		this.idBar = idBar;
		this.idProducto = idProducto;
	}

	public Integer getIdBar() {
		return idBar;
	}

	public void setIdBar(Integer idBar) {
		this.idBar = idBar;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	
	
}
