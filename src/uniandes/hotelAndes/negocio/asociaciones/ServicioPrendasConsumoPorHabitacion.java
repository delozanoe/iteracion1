package uniandes.hotelAndes.negocio.asociaciones;

public class ServicioPrendasConsumoPorHabitacion 
{
	private Integer idServicioPrendas;
	
	private Integer idConsumoPorHabitacion;

	public ServicioPrendasConsumoPorHabitacion(Integer idServicioPrendas, Integer idConsumoPorHabitacion) {
		super();
		this.idServicioPrendas = idServicioPrendas;
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}

	public Integer getIdServicioPrendas() {
		return idServicioPrendas;
	}

	public void setIdServicioPrendas(Integer idServicioPrendas) {
		this.idServicioPrendas = idServicioPrendas;
	}

	public Integer getIdConsumoPorHabitacion() {
		return idConsumoPorHabitacion;
	}

	public void setIdConsumoPorHabitacion(Integer idConsumoPorHabitacion) {
		this.idConsumoPorHabitacion = idConsumoPorHabitacion;
	}
	
	
}
