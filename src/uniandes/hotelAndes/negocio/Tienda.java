package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Tienda
{
	
	
	private String tipo;


	private int id;

	
	private Hotel hotel;

	
	
	private ArrayList<Producto> productos;

	private ArrayList<Producto> consumos;

	private ArrayList<ConsumoPorHabitacion> consumo;

	public Tienda(String tipo, int id, Hotel hotel, ArrayList<Producto> productos, ArrayList<Producto> consumos,
			ArrayList<ConsumoPorHabitacion> consumo) {
		super();
		this.tipo = tipo;
		this.id = id;
		this.hotel = hotel;
		this.productos = productos;
		this.consumos = consumos;
		this.consumo = consumo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	public ArrayList<Producto> getConsumos() {
		return consumos;
	}

	public void setConsumos(ArrayList<Producto> consumos) {
		this.consumos = consumos;
	}

	public ArrayList<ConsumoPorHabitacion> getConsumo() {
		return consumo;
	}

	public void setConsumo(ArrayList<ConsumoPorHabitacion> consumo) {
		this.consumo = consumo;
	}

	
	
}

