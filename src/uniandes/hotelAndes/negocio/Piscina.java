package uniandes.hotelAndes.negocio;
import java.util.Date;




public class Piscina
{
	
	private Integer id;
	private int capacidad;

	
	
	private double profundidad;

	
	
	private Date horaApertura;

	
	
	private Date horaCierre;

	
	
	private boolean costoIncluido;

	
	private double costoAdicional;

	
	private  Hotel hotel;


	public Piscina(int capacidad, double profundidad, Date horaApertura, Date horaCierre, boolean costoIncluido,
			double costoAdicional, Hotel hotel, Integer id) {
		super();
		this.id = id;
		this.capacidad = capacidad;
		this.profundidad = profundidad;
		this.horaApertura = horaApertura;
		this.horaCierre = horaCierre;
		this.costoIncluido = costoIncluido;
		this.costoAdicional = costoAdicional;
		this.hotel = hotel;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public int getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}


	public double getProfundidad() {
		return profundidad;
	}


	public void setProfundidad(double profundidad) {
		this.profundidad = profundidad;
	}


	public Date getHoraApertura() {
		return horaApertura;
	}


	public void setHoraApertura(Date horaApertura) {
		this.horaApertura = horaApertura;
	}


	public Date getHoraCierre() {
		return horaCierre;
	}


	public void setHoraCierre(Date horaCierre) {
		this.horaCierre = horaCierre;
	}


	public boolean isCostoIncluido() {
		return costoIncluido;
	}


	public void setCostoIncluido(boolean costoIncluido) {
		this.costoIncluido = costoIncluido;
	}


	public double getCostoAdicional() {
		return costoAdicional;
	}


	public void setCostoAdicional(double costoAdicional) {
		this.costoAdicional = costoAdicional;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	
 
}

