package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Producto
{
	private Integer id;
	
	private String nombre;

		
	private double costo;

	
	
	
	private boolean cargadoAHabitacion;

	
	private ArrayList<Bar> bar;

	
	
	private ArrayList<Restaurante> restaurantes;

	
	
	private Supermercado supermercado;


	private ArrayList<Tienda> tiendas;


	public Producto(String nombre, double costo, boolean cargadoAHabitacion, ArrayList<Bar> bar,
			ArrayList<Restaurante> restaurantes, Supermercado supermercado, ArrayList<Tienda> tiendas, Integer id) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.costo = costo;
		this.cargadoAHabitacion = cargadoAHabitacion;
		this.bar = bar;
		this.restaurantes = restaurantes;
		this.supermercado = supermercado;
		this.tiendas = tiendas;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public double getCosto() {
		return costo;
	}


	public void setCosto(double costo) {
		this.costo = costo;
	}


	public boolean isCargadoAHabitacion() {
		return cargadoAHabitacion;
	}


	public void setCargadoAHabitacion(boolean cargadoAHabitacion) {
		this.cargadoAHabitacion = cargadoAHabitacion;
	}


	public ArrayList<Bar> getBar() {
		return bar;
	}


	public void setBar(ArrayList<Bar> bar) {
		this.bar = bar;
	}


	public ArrayList<Restaurante> getRestaurantes() {
		return restaurantes;
	}


	public void setRestaurantes(ArrayList<Restaurante> restaurantes) {
		this.restaurantes = restaurantes;
	}


	public Supermercado getSupermercado() {
		return supermercado;
	}


	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}


	public ArrayList<Tienda> getTiendas() {
		return tiendas;
	}


	public void setTiendas(ArrayList<Tienda> tiendas) {
		this.tiendas = tiendas;
	}
	
	

	
}

