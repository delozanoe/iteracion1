package uniandes.hotelAndes.negocio;




public class ServicioPrendas
{

	private Integer id;
	private String tipo;
 
	private int numeroPrendas;


	private double costoTotal;

	
	private Hotel hotel;


	
	public ServicioPrendas(Integer id, String tipo, int numeroPrendas, double costoTotal, Hotel hotel) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.numeroPrendas = numeroPrendas;
		this.costoTotal = costoTotal;
		this.hotel = hotel;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public int getNumeroPrendas() {
		return numeroPrendas;
	}


	public void setNumeroPrendas(int numeroPrendas) {
		this.numeroPrendas = numeroPrendas;
	}


	public double getCostoTotal() {
		return costoTotal;
	}


	public void setCostoTotal(double costoTotal) {
		this.costoTotal = costoTotal;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	
	

	

}

