package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Restaurante
{
	private int capacidad;

	
	
	private String estilo;

	
	private  Hotel hotel;

	
	
	private  ArrayList<Producto> cartaProductos;


	private ArrayList<Producto> consumos;


	public Restaurante(int capacidad, String estilo, Hotel hotel, ArrayList<Producto> cartaProductos,
			ArrayList<Producto> consumos) {
		super();
		this.capacidad = capacidad;
		this.estilo = estilo;
		this.hotel = hotel;
		this.cartaProductos = cartaProductos;
		this.consumos = consumos;
	}


	public int getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}


	public String getEstilo() {
		return estilo;
	}


	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}


	public ArrayList<Producto> getCartaProductos() {
		return cartaProductos;
	}


	public void setCartaProductos(ArrayList<Producto> cartaProductos) {
		this.cartaProductos = cartaProductos;
	}


	public ArrayList<Producto> getConsumos() {
		return consumos;
	}


	public void setConsumos(ArrayList<Producto> consumos) {
		this.consumos = consumos;
	}

	
}

