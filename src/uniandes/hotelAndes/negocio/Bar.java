package uniandes.hotelAndes.negocio;
import java.util.ArrayList;
import java.util.Set;



public class Bar
{
	
	private Integer id;
	
	private int capacidad;

	private String estilo;

	
	
	private ArrayList<Producto> cartaProductos;


	private Hotel hotel;

	
	private  ArrayList<Producto> consumos;


	public Bar(int capacidad, String estilo, ArrayList<Producto> cartaProductos, Hotel hotel, ArrayList<Producto> consumos, Integer id) {
		super();
		this.id =id;
		this.capacidad = capacidad;
		this.estilo = estilo;
		this.cartaProductos = cartaProductos;
		this.hotel = hotel;
		this.consumos = consumos;
	}
	
	


	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public int getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}


	public String getEstilo() {
		return estilo;
	}


	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}


	public ArrayList<Producto> getCartaProductos() {
		return cartaProductos;
	}


	public void setCartaProductos(ArrayList<Producto> cartaProductos) {
		this.cartaProductos = cartaProductos;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}


	public ArrayList<Producto> getConsumos() {
		return consumos;
	}


	public void setConsumos(ArrayList<Producto> consumos) {
		this.consumos = consumos;
	}

	
	
}

