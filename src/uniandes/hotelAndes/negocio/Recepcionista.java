package uniandes.hotelAndes.negocio;




public class Recepcionista extends Usuario
{

	
	private Hotel hotel;

	public Recepcionista(Hotel hotel) {
		super(nombre, tipoDocumento, numeroDocumento, correo, id);
		this.hotel = hotel;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	

	
}

