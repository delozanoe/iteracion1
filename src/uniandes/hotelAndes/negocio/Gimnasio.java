package uniandes.hotelAndes.negocio;
import java.util.Date;




public class Gimnasio
{
	
	private Integer id;
	private int capacidad;

	
	private String maquinas;

	
	private Date horarioApertura;


	private Date horarioCierre;


	
	private boolean costoIncluido;


	
	private double costoAdicional;


	private  Hotel hotel;


	public Gimnasio(int capacidad, String maquinas, Date horarioApertura, Date horarioCierre, boolean costoIncluido,
			double costoAdicional, Hotel hotel, Integer id) {
		super();
		this.id = id;
		this.capacidad = capacidad;
		this.maquinas = maquinas;
		this.horarioApertura = horarioApertura;
		this.horarioCierre = horarioCierre;
		this.costoIncluido = costoIncluido;
		this.costoAdicional = costoAdicional;
		this.hotel = hotel;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public int getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}


	public String getMaquinas() {
		return maquinas;
	}


	public void setMaquinas(String maquinas) {
		this.maquinas = maquinas;
	}


	public Date getHorarioApertura() {
		return horarioApertura;
	}


	public void setHorarioApertura(Date horarioApertura) {
		this.horarioApertura = horarioApertura;
	}


	public Date getHorarioCierre() {
		return horarioCierre;
	}


	public void setHorarioCierre(Date horarioCierre) {
		this.horarioCierre = horarioCierre;
	}


	public boolean isCostoIncluido() {
		return costoIncluido;
	}


	public void setCostoIncluido(boolean costoIncluido) {
		this.costoIncluido = costoIncluido;
	}


	public double getCostoAdicional() {
		return costoAdicional;
	}


	public void setCostoAdicional(double costoAdicional) {
		this.costoAdicional = costoAdicional;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	

}

