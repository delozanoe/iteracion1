package uniandes.hotelAndes.negocio;
import java.util.ArrayList;





public class Cliente extends Usuario
{
	
	
	
	private boolean pazYSalvo;


	private Habitacion habitacion;


	
	private ArrayList<ReservaHabitacion> reserva;


	
	private ArrayList<Reserva> reservas;


	
	private ArrayList<PlanConsumo> planConsumo;



	public Cliente(boolean pazYSalvo, Habitacion habitacion, ArrayList<ReservaHabitacion> reserva,
			ArrayList<Reserva> reservas, ArrayList<PlanConsumo> planConsumo) {
		
		super(nombre, tipoDocumento, numeroDocumento, correo, id);
		this.id = id; 
		this.pazYSalvo = pazYSalvo;
		this.habitacion = habitacion;
		this.reserva = reserva;
		this.reservas = reservas;
		this.planConsumo = planConsumo;
	}

	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public boolean isPazYSalvo() {
		return pazYSalvo;
	}



	public void setPazYSalvo(boolean pazYSalvo) {
		this.pazYSalvo = pazYSalvo;
	}



	public Habitacion getHabitacion() {
		return habitacion;
	}



	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}



	public ArrayList<ReservaHabitacion> getReserva() {
		return reserva;
	}



	public void setReserva(ArrayList<ReservaHabitacion> reserva) {
		this.reserva = reserva;
	}



	public ArrayList<Reserva> getReservas() {
		return reservas;
	}



	public void setReservas(ArrayList<Reserva> reservas) {
		this.reservas = reservas;
	}



	public ArrayList<PlanConsumo> getPlanConsumo() {
		return planConsumo;
	}



	public void setPlanConsumo(ArrayList<PlanConsumo> planConsumo) {
		this.planConsumo = planConsumo;
	}

	

}

