package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Habitacion
{

	private Integer id; 
	private int capacidad;


	private TipoHabitacion tipoDeHabitacion;

	private double costoPorNoche;
	
	private double cuenta;
	
	private Hotel hotel;
	
	private ArrayList<Cliente> clientes;

	
	private ConsumoPorHabitacion consumoHabitacion;


	public Habitacion(int capacidad, TipoHabitacion tipoDeHabitacion, double costoPorNoche, double cuenta, Hotel hotel,
			ArrayList<Cliente> clientes, ConsumoPorHabitacion consumoHabitacion, Integer id) {
		super();
		this.id = id;
		this.capacidad = capacidad;
		this.tipoDeHabitacion = tipoDeHabitacion;
		this.costoPorNoche = costoPorNoche;
		this.cuenta = cuenta;
		this.hotel = hotel;
		this.clientes = clientes;
		this.consumoHabitacion = consumoHabitacion;
	}


	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public int getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}


	public TipoHabitacion getTipoDeHabitacion() {
		return tipoDeHabitacion;
	}


	public void setTipoDeHabitacion(TipoHabitacion tipoDeHabitacion) {
		this.tipoDeHabitacion = tipoDeHabitacion;
	}


	public double getCostoPorNoche() {
		return costoPorNoche;
	}


	public void setCostoPorNoche(double costoPorNoche) {
		this.costoPorNoche = costoPorNoche;
	}


	public double getCuenta() {
		return cuenta;
	}


	public void setCuenta(double cuenta) {
		this.cuenta = cuenta;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}


	public ArrayList<Cliente> getClientes() {
		return clientes;
	}


	public void setClientes(ArrayList<Cliente> clientes) {
		this.clientes = clientes;
	}


	public ConsumoPorHabitacion getConsumoHabitacion() {
		return consumoHabitacion;
	}


	public void setConsumoHabitacion(ConsumoPorHabitacion consumoHabitacion) {
		this.consumoHabitacion = consumoHabitacion;
	}
	
	
	
	
	

}

