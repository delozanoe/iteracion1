package uniandes.hotelAndes.negocio;



public class Usuario
{

	protected static Integer id;
	protected static String nombre;


	
	protected static String tipoDocumento;

	protected static long numeroDocumento;

	
	protected static String correo;


	public Usuario(String nombre, String tipoDocumento, long numeroDocumento, String correo, Integer id) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.correo = correo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getTipoDocumento() {
		return tipoDocumento;
	}


	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}


	public long getNumeroDocumento() {
		return numeroDocumento;
	}


	public void setNumeroDocumento(long numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}


	public String getCorreo() {
		return correo;
	}


	public void setCorreo(String correo) {
		this.correo = correo;
	}

	

}

