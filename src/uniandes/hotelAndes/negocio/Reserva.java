package uniandes.hotelAndes.negocio;

import java.util.ArrayList;
import java.util.Date;

public class Reserva
{
	private Integer id;
	
	private Date horaInicio;

	
	private int duracion;


	
	private Date dia;

	
	
	

	
	private String lugar;

	
	
	private Cliente cliente;

	
	private ServicioSpa servicio;

	
	
	private ArrayList<Salon> salones;



	public Reserva(Date horaInicio, int duracion, Date dia, int id, String lugar, Cliente cliente, ServicioSpa servicio,
			ArrayList<Salon> salones) {
		super();
		this.horaInicio = horaInicio;
		this.duracion = duracion;
		this.dia = dia;
		this.id = id;
		this.lugar = lugar;
		this.cliente = cliente;
		this.servicio = servicio;
		this.salones = salones;
	}



	public Date getHoraInicio() {
		return horaInicio;
	}



	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}



	public int getDuracion() {
		return duracion;
	}



	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}



	public Date getDia() {
		return dia;
	}



	public void setDia(Date dia) {
		this.dia = dia;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getLugar() {
		return lugar;
	}



	public void setLugar(String lugar) {
		this.lugar = lugar;
	}



	public Cliente getCliente() {
		return cliente;
	}



	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}



	public ServicioSpa getServicio() {
		return servicio;
	}



	public void setServicio(ServicioSpa servicio) {
		this.servicio = servicio;
	}



	public ArrayList<Salon> getSalones() {
		return salones;
	}



	public void setSalones(ArrayList<Salon> salones) {
		this.salones = salones;
	}
	
	
	
	

}

