package uniandes.hotelAndes.negocio;



public class ServicioSpa
{
	private Integer id;
	
	private String nombre;

	
	
	private double costo;


	
	private int duracion;


	
	private boolean cargadoAHabitacion;


	
	private Spa spa;



	public ServicioSpa(String nombre, double costo, int duracion, boolean cargadoAHabitacion, Spa spa, Integer id) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.costo = costo;
		this.duracion = duracion;
		this.cargadoAHabitacion = cargadoAHabitacion;
		this.spa = spa;
	}

	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public double getCosto() {
		return costo;
	}



	public void setCosto(double costo) {
		this.costo = costo;
	}



	public int getDuracion() {
		return duracion;
	}



	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}



	public boolean isCargadoAHabitacion() {
		return cargadoAHabitacion;
	}



	public void setCargadoAHabitacion(boolean cargadoAHabitacion) {
		this.cargadoAHabitacion = cargadoAHabitacion;
	}



	public Spa getSpa() {
		return spa;
	}



	public void setSpa(Spa spa) {
		this.spa = spa;
	}

	
	

}

