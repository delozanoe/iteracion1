package uniandes.hotelAndes.negocio;



public class Prestamo
{
	
	private Integer id;
	
	private String utensilio;

	
	private boolean devuelto;

	
	private double costo;

	public Hotel hotel;

	public Prestamo(String utensilio, boolean devuelto, double costo, Hotel hotel, Integer id) {
		super();
		this.id = id;
		this.utensilio = utensilio;
		this.devuelto = devuelto;
		this.costo = costo;
		this.hotel = hotel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUtensilio() {
		return utensilio;
	}

	public void setUtensilio(String utensilio) {
		this.utensilio = utensilio;
	}

	public boolean isDevuelto() {
		return devuelto;
	}

	public void setDevuelto(boolean devuelto) {
		this.devuelto = devuelto;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	
	
}

