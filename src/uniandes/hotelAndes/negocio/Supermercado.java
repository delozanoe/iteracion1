package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Supermercado
{
	
	private Integer id;
	
	private Hotel hotel;

	
	
	private ArrayList<Producto> productos;

	
	
	private ArrayList<Producto> consumos;



	public Supermercado(Hotel hotel, ArrayList<Producto> productos, ArrayList<Producto> consumos, Integer id) {
		super();
		this.id= id;
		this.hotel = hotel;
		this.productos = productos;
		this.consumos = consumos;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public Hotel getHotel() {
		return hotel;
	}



	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}



	public ArrayList<Producto> getProductos() {
		return productos;
	}



	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}



	public ArrayList<Producto> getConsumos() {
		return consumos;
	}



	public void setConsumos(ArrayList<Producto> consumos) {
		this.consumos = consumos;
	}

	

}

