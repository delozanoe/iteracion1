package uniandes.hotelAndes.negocio;

/**
 * 
 * @author de.lozanoe
 *
 */
public class AdministradorDatos extends Usuario
{
	/**
	 * 
	 */
	private Hotel hotel;



	public AdministradorDatos(Hotel hotel) {
		super(nombre, tipoDocumento, numeroDocumento, correo, id);
		this.hotel = hotel;
	}
	
	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

}

