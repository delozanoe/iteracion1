package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Salon
{

	private Integer id;
	
	private int capacidad;


	private double costoPorHora;

	
	
	private boolean usoEquipos;

	
	
	private double costoAdicional;

	
	
	private String tipo;

	
	
	private Hotel hotel;

	
	private ArrayList<Reserva> reservas;


	public Salon(int capacidad, double costoPorHora, boolean usoEquipos, double costoAdicional, String tipo,
			Hotel hotel, ArrayList<Reserva> reservas, Integer id) {
		super();
		this.id = id; 
		this.capacidad = capacidad;
		this.costoPorHora = costoPorHora;
		this.usoEquipos = usoEquipos;
		this.costoAdicional = costoAdicional;
		this.tipo = tipo;
		this.hotel = hotel;
		this.reservas = reservas;
	}


	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public int getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}


	public double getCostoPorHora() {
		return costoPorHora;
	}


	public void setCostoPorHora(double costoPorHora) {
		this.costoPorHora = costoPorHora;
	}


	public boolean isUsoEquipos() {
		return usoEquipos;
	}


	public void setUsoEquipos(boolean usoEquipos) {
		this.usoEquipos = usoEquipos;
	}


	public double getCostoAdicional() {
		return costoAdicional;
	}


	public void setCostoAdicional(double costoAdicional) {
		this.costoAdicional = costoAdicional;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}


	public ArrayList<Reserva> getReservas() {
		return reservas;
	}


	public void setReservas(ArrayList<Reserva> reservas) {
		this.reservas = reservas;
	}
	
	

	

}

