package uniandes.hotelAndes.negocio;

import java.sql.Date;
import java.util.ArrayList;

public class ReservaHabitacion
{
	
	private Integer id;
	private Date fechaEntrada;

	
	
	private Date fechaSalida;

	
	
	private int numeroPersonas;

	
	private Hotel hotel;


	
	private ArrayList<FormaDePago> formaDePago;

	
	
	private ArrayList<Cliente> clientes;



	public ReservaHabitacion(Date fechaEntrada, Date fechaSalida, int numeroPersonas, Hotel hotel,
			ArrayList<FormaDePago> formaDePago, ArrayList<Cliente> clientes, Integer id)
	{
		super();
		this.id = id;
		this.fechaEntrada = fechaEntrada;
		this.fechaSalida = fechaSalida;
		this.numeroPersonas = numeroPersonas;
		this.hotel = hotel;
		this.formaDePago = formaDePago;
		this.clientes = clientes;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public Date getFechaEntrada() {
		return fechaEntrada;
	}



	public void setFechaEntrada(Date fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}



	public Date getFechaSalida() {
		return fechaSalida;
	}



	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}



	public int getNumeroPersonas() {
		return numeroPersonas;
	}



	public void setNumeroPersonas(int numeroPersonas) {
		this.numeroPersonas = numeroPersonas;
	}



	public Hotel getHotel() {
		return hotel;
	}



	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}



	public ArrayList<FormaDePago> getFormaDePago() {
		return formaDePago;
	}



	public void setFormaDePago(ArrayList<FormaDePago> formaDePago) {
		this.formaDePago = formaDePago;
	}



	public ArrayList<Cliente> getClientes() {
		return clientes;
	}



	public void setClientes(ArrayList<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	

	

}

