package uniandes.hotelAndes.negocio;


public enum TipoHabitacion
{
	SUITEPRESIDENCIAL, SUITE, FAMILIAR, DOBLE, SENCILLA;
}
