package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Hotel
{

	private Integer id;
	
	private String pais;

	
	private String ciudad;

	
	private int ofertaHabitacional;

	
	private String direccion;

	
	private ArrayList<Piscina> piscinas;


	
	private ArrayList<Gimnasio> gimnasio;

	
	
	private Internet internet;

	
	
	private ArrayList<Bar> bares;


	private ArrayList<Restaurante> restaurantes;

	
	private Supermercado supermercado;

	
	private ArrayList<Tienda> tiendas;

	
	private Spa spa;

	
	private ArrayList<ServicioPrendas> serviciosPrendas;

	
	private ArrayList<Prestamo> prestamos;

	
	private ArrayList<Habitacion> habitaciones;


	
	private ArrayList<Salon> salon;

	
	private ArrayList<ReservaHabitacion> reservas;


	private ArrayList<Recepcionista> recepcionistas;

	
	private ArrayList<Empleado> empleados;

	
	
	private AdministradorDatos administradorDatos;

	
	private Gerente gerente;


	public Hotel(String pais, String ciudad, int ofertaHabitacional, String direccion, ArrayList<Piscina> piscinas,
			ArrayList<Gimnasio> gimnasio, Internet internet, ArrayList<Bar> bares, ArrayList<Restaurante> restaurantes,
			Supermercado supermercado, ArrayList<Tienda> tiendas, Spa spa, ArrayList<ServicioPrendas> serviciosPrendas,
			ArrayList<Prestamo> prestamos, ArrayList<Habitacion> habitaciones, ArrayList<Salon> salon,
			ArrayList<ReservaHabitacion> reservas, ArrayList<Recepcionista> recepcionistas,
			ArrayList<Empleado> empleados, AdministradorDatos administradorDatos, Gerente gerente, Integer id) {
		super();
		this.id = id;
		this.pais = pais;
		this.ciudad = ciudad;
		this.ofertaHabitacional = ofertaHabitacional;
		this.direccion = direccion;
		this.piscinas = piscinas;
		this.gimnasio = gimnasio;
		this.internet = internet;
		this.bares = bares;
		this.restaurantes = restaurantes;
		this.supermercado = supermercado;
		this.tiendas = tiendas;
		this.spa = spa;
		this.serviciosPrendas = serviciosPrendas;
		this.prestamos = prestamos;
		this.habitaciones = habitaciones;
		this.salon = salon;
		this.reservas = reservas;
		this.recepcionistas = recepcionistas;
		this.empleados = empleados;
		this.administradorDatos = administradorDatos;
		this.gerente = gerente;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getPais() {
		return pais;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public int getOfertaHabitacional() {
		return ofertaHabitacional;
	}


	public void setOfertaHabitacional(int ofertaHabitacional) {
		this.ofertaHabitacional = ofertaHabitacional;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public ArrayList<Piscina> getPiscinas() {
		return piscinas;
	}


	public void setPiscinas(ArrayList<Piscina> piscinas) {
		this.piscinas = piscinas;
	}


	public ArrayList<Gimnasio> getGimnasio() {
		return gimnasio;
	}


	public void setGimnasio(ArrayList<Gimnasio> gimnasio) {
		this.gimnasio = gimnasio;
	}


	public Internet getInternet() {
		return internet;
	}


	public void setInternet(Internet internet) {
		this.internet = internet;
	}


	public ArrayList<Bar> getBares() {
		return bares;
	}


	public void setBares(ArrayList<Bar> bares) {
		this.bares = bares;
	}


	public ArrayList<Restaurante> getRestaurantes() {
		return restaurantes;
	}


	public void setRestaurantes(ArrayList<Restaurante> restaurantes) {
		this.restaurantes = restaurantes;
	}


	public Supermercado getSupermercado() {
		return supermercado;
	}


	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}


	public ArrayList<Tienda> getTiendas() {
		return tiendas;
	}


	public void setTiendas(ArrayList<Tienda> tiendas) {
		this.tiendas = tiendas;
	}


	public Spa getSpa() {
		return spa;
	}


	public void setSpa(Spa spa) {
		this.spa = spa;
	}


	public ArrayList<ServicioPrendas> getServiciosPrendas() {
		return serviciosPrendas;
	}


	public void setServiciosPrendas(ArrayList<ServicioPrendas> serviciosPrendas) {
		this.serviciosPrendas = serviciosPrendas;
	}


	public ArrayList<Prestamo> getPrestamos() {
		return prestamos;
	}


	public void setPrestamos(ArrayList<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}


	public ArrayList<Habitacion> getHabitaciones() {
		return habitaciones;
	}


	public void setHabitaciones(ArrayList<Habitacion> habitaciones) {
		this.habitaciones = habitaciones;
	}


	public ArrayList<Salon> getSalon() {
		return salon;
	}


	public void setSalon(ArrayList<Salon> salon) {
		this.salon = salon;
	}


	public ArrayList<ReservaHabitacion> getReservas() {
		return reservas;
	}


	public void setReservas(ArrayList<ReservaHabitacion> reservas) {
		this.reservas = reservas;
	}


	public ArrayList<Recepcionista> getRecepcionistas() {
		return recepcionistas;
	}


	public void setRecepcionistas(ArrayList<Recepcionista> recepcionistas) {
		this.recepcionistas = recepcionistas;
	}


	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}


	public void setEmpleados(ArrayList<Empleado> empleados) {
		this.empleados = empleados;
	}


	public AdministradorDatos getAdministradorDatos() {
		return administradorDatos;
	}


	public void setAdministradorDatos(AdministradorDatos administradorDatos) {
		this.administradorDatos = administradorDatos;
	}


	public Gerente getGerente() {
		return gerente;
	}


	public void setGerente(Gerente gerente) {
		this.gerente = gerente;
	}
	
	
	
	

}

