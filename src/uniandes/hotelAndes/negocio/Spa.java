package uniandes.hotelAndes.negocio;

import java.util.ArrayList;

public class Spa
{
	
	private Integer id;
	private int capacidad;

	
	
	private Hotel hotel;

	
	
	private ArrayList<ServicioSpa> servicios;

	
	
	private ArrayList<ServicioSpa> consumos;

	private ArrayList<Reserva> reservas;

	public Spa(int capacidad, Hotel hotel, ArrayList<ServicioSpa> servicios, ArrayList<ServicioSpa> consumos,
			ArrayList<Reserva> reservas, Integer id) {
		super();
		this.id =id;
		this.capacidad = capacidad;
		this.hotel = hotel;
		this.servicios = servicios;
		this.consumos = consumos;
		this.reservas = reservas;
	}

	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public ArrayList<ServicioSpa> getServicios() {
		return servicios;
	}

	public void setServicios(ArrayList<ServicioSpa> servicios) {
		this.servicios = servicios;
	}

	public ArrayList<ServicioSpa> getConsumos() {
		return consumos;
	}

	public void setConsumos(ArrayList<ServicioSpa> consumos) {
		this.consumos = consumos;
	}

	public ArrayList<Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(ArrayList<Reserva> reservas) {
		this.reservas = reservas;
	}
	
	


}

