package uniandes.hotelAndes.negocio;



public class Internet
{
	private Integer id; 
	
	private double capacidad;

	
	private boolean costoIncluido;


	
	private double costoPorDia;


	private Hotel hotel;


	public Internet(double capacidad, boolean costoIncluido, double costoPorDia, Hotel hotel, Integer id) { 
		super();
		this.id = id;
		this.capacidad = capacidad;
		this.costoIncluido = costoIncluido;
		this.costoPorDia = costoPorDia;
		this.hotel = hotel;
	}

	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public double getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(double capacidad) {
		this.capacidad = capacidad;
	}


	public boolean isCostoIncluido() {
		return costoIncluido;
	}


	public void setCostoIncluido(boolean costoIncluido) {
		this.costoIncluido = costoIncluido;
	}


	public double getCostoPorDia() {
		return costoPorDia;
	}


	public void setCostoPorDia(double costoPorDia) {
		this.costoPorDia = costoPorDia;
	}


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	

}

