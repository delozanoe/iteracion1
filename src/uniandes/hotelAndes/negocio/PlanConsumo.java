package uniandes.hotelAndes.negocio;



public class PlanConsumo
{
	 private Integer id; 
	 
	 private Hotel hotel;

	public PlanConsumo(Integer id, Hotel hotel) {
		super();
		this.id = id;
		this.hotel = hotel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	 
}

