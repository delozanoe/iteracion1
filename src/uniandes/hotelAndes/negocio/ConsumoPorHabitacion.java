package uniandes.hotelAndes.negocio;
import java.util.ArrayList;


public class ConsumoPorHabitacion
{
	private Integer id;
	
	private double valorTotal;


	
	private ArrayList<Gimnasio> gimnasio;


	
	private ArrayList<ServicioPrendas> servicioPrendas;


	
	private ArrayList<Piscina> piscina;

	
	private ArrayList<Bar> bar;

	private ArrayList<Supermercado> supermercado;

	
	private ArrayList<Internet> internet;


	
	private ArrayList<Spa> spa;


	
	private ArrayList<Salon> salones;


	
	private Habitacion habitacion;



	public ConsumoPorHabitacion(double valorTotal, ArrayList<Gimnasio> gimnasio,
			ArrayList<ServicioPrendas> servicioPrendas, ArrayList<Piscina> piscina, ArrayList<Bar> bar,
			ArrayList<Supermercado> supermercado, ArrayList<Internet> internet, ArrayList<Spa> spa,
			ArrayList<Salon> salones, Habitacion habitacion, Integer id ) {
		super();
		this.id = id;
		this.valorTotal = valorTotal;
		this.gimnasio = gimnasio;
		this.servicioPrendas = servicioPrendas;
		this.piscina = piscina;
		this.bar = bar;
		this.supermercado = supermercado;
		this.internet = internet;
		this.spa = spa;
		this.salones = salones;
		this.habitacion = habitacion;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public double getValorTotal() {
		return valorTotal;
	}



	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}



	public ArrayList<Gimnasio> getGimnasio() {
		return gimnasio;
	}



	public void setGimnasio(ArrayList<Gimnasio> gimnasio) {
		this.gimnasio = gimnasio;
	}



	public ArrayList<ServicioPrendas> getServicioPrendas() {
		return servicioPrendas;
	}



	public void setServicioPrendas(ArrayList<ServicioPrendas> servicioPrendas) {
		this.servicioPrendas = servicioPrendas;
	}



	public ArrayList<Piscina> getPiscina() {
		return piscina;
	}



	public void setPiscina(ArrayList<Piscina> piscina) {
		this.piscina = piscina;
	}



	public ArrayList<Bar> getBar() {
		return bar;
	}



	public void setBar(ArrayList<Bar> bar) {
		this.bar = bar;
	}



	public ArrayList<Supermercado> getSupermercado() {
		return supermercado;
	}



	public void setSupermercado(ArrayList<Supermercado> supermercado) {
		this.supermercado = supermercado;
	}



	public ArrayList<Internet> getInternet() {
		return internet;
	}



	public void setInternet(ArrayList<Internet> internet) {
		this.internet = internet;
	}



	public ArrayList<Spa> getSpa() {
		return spa;
	}



	public void setSpa(ArrayList<Spa> spa) {
		this.spa = spa;
	}



	public ArrayList<Salon> getSalones() {
		return salones;
	}



	public void setSalones(ArrayList<Salon> salones) {
		this.salones = salones;
	}



	public Habitacion getHabitacion() {
		return habitacion;
	}



	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}

	

}

