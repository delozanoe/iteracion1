package uniandes.hotelAndes.persitencia.negocio;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.hotelAndes.negocio.Bar;



class SQLBar 
{
	private final static String SQL = PersistenciaHotelAndes.SQL;

	private PersistenciaHotelAndes pha;

	public SQLBar(PersistenciaHotelAndes pha)
	{
		this.pha = pha;
	}
	public long adicionarBar (PersistenceManager pm, Integer idBar, int  capacidad, String estilo, Integer idHotel) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + pha.getSqlBar() + "(id, capacidad, estilo, idHotel) values ( ?, ?, ?, ?)");
		q.setParameters(idBar, capacidad, estilo, idHotel);
		return (long) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UN BAR de la base de datos de Parranderos, por su identificador
	 * @param pm - El manejador de persistencia
	 * @param idBar - El identificador del bar
	 * @return EL n�mero de tuplas eliminadas
	 */
	public long eliminarBarPorId (PersistenceManager pm, Integer idBar)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + pha.getSqlBar() + " WHERE id = ?");
		q.setParameters(idBar);
		return (long) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la informaci�n de UN BAR de la 
	 * base de datos de Parranderos, por su identificador
	 * @param pm - El manejador de persistencia
	 * @param idBar - El identificador del bar
	 * @return El objeto BAR que tiene el identificador dado
	 */
	public Bar darBarPorId (PersistenceManager pm, Integer idBar) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " +pha.getSqlBar() + " WHERE id = ?");
		q.setResultClass(Bar.class);
		q.setParameters(idBar);
		return (Bar) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la informaci�n de LOS BARES de la 
	 * base de datos de Parranderos
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos BAR
	 */
	public List<Bar> darBares (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pha.getSqlBar());
		q.setResultClass(Bar.class);
		return (List<Bar>) q.executeList();
	}



}


