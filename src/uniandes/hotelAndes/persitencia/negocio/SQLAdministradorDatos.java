package uniandes.hotelAndes.persitencia.negocio;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.hotelAndes.negocio.AdministradorDatos;
import uniandes.hotelAndes.negocio.Hotel;

class SQLAdministradorDatos 
{

	private final static String SQL = PersistenciaHotelAndes.SQL;
	
	private PersistenciaHotelAndes pha;
	
	public SQLAdministradorDatos(PersistenciaHotelAndes pha)
	{
		this.pha = pha;
	}
	
	public long adicionarAdministradorDatos (PersistenceManager pm, Integer idAdmistradorDatos, String nombre, String tipoDocumento, long numeroDocumento,String correo) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + pha.getSqlAdministradorDatos() + "(id, nombre, tipoDocumento, numeroDocumento, correo) values (?, ?, ?, ?, ?)");
        q.setParameters(idAdmistradorDatos, nombre, tipoDocumento, numeroDocumento, correo);
        return (long) q.executeUnique();
	}
	
	public long eliminarAdministradorPorNombre (PersistenceManager pm, String nombreAdministradorDatos)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + pha.getSqlAdministradorDatos () + " WHERE nombre = ?");
        q.setParameters(nombreAdministradorDatos);
        return (long) q.executeUnique();
	}
	
	public long eliminarAdministradorPorId (PersistenceManager pm, Integer idBar)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + pha.getSqlAdministradorDatos () + " WHERE id = ?");
        q.setParameters(idBar);
        return (long) q.executeUnique();
	}


	public AdministradorDatos darAdministradorPorDocumento (PersistenceManager pm, long numDocumento) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pha.getSqlAdministradorDatos() + " WHERE id = ?");
		q.setResultClass(AdministradorDatos.class);
		q.setParameters(numDocumento);
		return (AdministradorDatos) q.executeUnique();
	}

	public List<AdministradorDatos> darAdministradores (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + pha.getSqlAdministradorDatos ());
		q.setResultClass(AdministradorDatos.class);
		return (List<AdministradorDatos>) q.executeList();
	}

	
	

}
