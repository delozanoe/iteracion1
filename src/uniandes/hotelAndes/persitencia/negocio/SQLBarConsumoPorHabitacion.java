package uniandes.hotelAndes.persitencia.negocio;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

class SQLBarConsumoPorHabitacion
{
private final static String SQL = PersistenciaHotelAndes.SQL;
	
	private PersistenciaHotelAndes pha;
	
	public SQLBarConsumoPorHabitacion(PersistenciaHotelAndes pha)
	{
		this.pha = pha;
	}
	
	public long adicionarConsumoPorHabitacion(PersistenceManager pm, Integer idBar, Integer idHabitacion)
	{
		 Query q = pm.newQuery(SQL, "INSERT INTO " + pha.getSqlBarConsumoPorHabitacion() + "(idBar, idHabitacion) values (?, ?)");
	        q.setParameters(idBar, idHabitacion);
	        return (long) q.executeUnique();
	}
}
