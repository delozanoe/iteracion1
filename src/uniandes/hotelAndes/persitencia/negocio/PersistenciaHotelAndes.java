package uniandes.hotelAndes.persitencia.negocio;

import java.util.LinkedList;
import java.util.List;

import java.util.logging.Logger;

import javax.jdo.JDODataStoreException;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;



public class PersistenciaHotelAndes 
{
	private static Logger log = Logger.getLogger(PersistenciaHotelAndes.class.getName());

	/**
	 * Cadena para indicar el tipo de sentencias que se va a utilizar en una consulta
	 */
	public final static String SQL = "javax.jdo.query.SQL";
	
	private static PersistenciaHotelAndes instance;
	
	private PersistenceManagerFactory pmf;

	private List <String> tablas;
	
	
	
	private SQLAdministradorDatos sqlAdministradorDatos;
	
	private SQLBar sqlBar; 
	
	private SQLBarConsumoPorHabitacion sqlBarConsumoPorHabitacion;
	
	private SQLBarSirveProducto sqlBarSirveProducto;
	
	private SQLBarVendioProducto sqlBarVendioProducto;
	
	private SQLCLIENTE sqlCliente; 
	
	private SQLConsumoPorHabitacion sqlConsumoPorHabitacion;
	
	private SQLEmpleado sqlEmpleado;
	
	private SQLFormaDePago sqlFormaDePago;
	
	private SQLGerente sqlGerente;
	
	private SQLGimnasio sqlGimnasio;
	
	private SQLGimnasioConsumoPorHabitacion sqlGimnasioConsumoPorHabitacion;
	
	private SQLHabitacion sqlHabitacion;
	
	private SQLHotel sqlHotel;
	
	private SQLInternet sqlInternet; 
	
	private SQLInternetConsumoPorHabitacion sqlInternetConsumoPorHabitacion;
	
	private SQLPiscina sqlPiscina;
	
	private SQLPlanConsumo sqlPlanConsumo; 
	
	private SQLPrestamo sqlPrestamo;
	
	private SQLPrestamoConsumoPorHabitacion sqlPrestamoConsumoPorHabitacion;
	
	private SQLProductoConsumoPorHabitacion sqlProductoConsumoPorHabitacion;
	
	private SQLRecepcionista sqlRecepcionista; 
	
	private SQLReserva sqlReserva; 
	
	private SQLReservaHabitacion sqlReservaHabitacion;
	
	private SQLRestaurante sqlRestaurante;  
	
	private SQLRestauranteSirveProducto sqlRestauranteSirveProducto;
	
	private SQLRestauranteVendioProducto sqlRestauranteVendioProducto;
	
	private SQLSalon sqlSalon;
	
	private SQLSalonConsumoPorHabitacion sqlSalonConsumoPorHabitacion;
	
	private SQLSalonReserva sqlSalonReserva;
	
	private SQLServicioPrendas sqlServicioPrendas; 
	
	private SQLServicioPrendasConsumoPorHabitacion sqlServicioPrendasConsumoPorHabitacion;
	
	private SQLServicioSpa sqlServicioSpa; 
	
	private SQLSPA sqlSpa;
	
	private SQLSpaConsumoPorHabitacion sqlSpaConsumoPorHabitacion;
	
	private SQLSupermercado sqlSupermercado; 
	
	private SQLSupermercadoConsumoPorHabitacion sqlSupermercadoConsumoPorHabitacion;
	
	private SQLTIENDA sqlTienda;
	
	private SQLTiendaConsumoPorHabitacion sqlTiendaConsumoPorHabitacion;
	
	private SQLTiendaVendeProducto sqlTiendaVendeProducto;
	
	private SQLTiendaVendioProducto sqlTiendaVendioProducto;
		
	private SQLTipoHabitacion sqlTipoHabitacion;
	
	private SQLUsuario sqlUsuario;
	
	private SQLUtil sqlUtil;
	
	
	
	private PersistenciaHotelAndes()
	{
		pmf = JDOHelper.getPersistenceManagerFactory("ISIS2304A211910");
		crearCalsesSQL();
		
		tablas = new LinkedList<String>();
		tablas.add("");
		tablas.add("ADMINISTRADORDATOS");
		tablas.add("BAR");
		tablas.add("BARCONSUMOPORHABITACION");
		tablas.add("BARSIRVEPRODUCTO");
		tablas.add("BARVENDIOPRODUCTO");
		tablas.add("CLIENTE");
		tablas.add("CONSUMOPORHABITACION");
		tablas.add("EMPLEADO");
		tablas.add("FORMADEPAGO");
		tablas.add("GERENTE");
		
		tablas.add("GIMNASIO");
		tablas.add("GIMNASIOCONSUMOPORHABITACION");
		tablas.add("HABITACION");
		tablas.add("HOTEL");
		tablas.add("INTERNET");
		tablas.add("INTERNETCONSUMOPORHABITACION");
		tablas.add("PISCINA");
		tablas.add("PLANCONSUMO");
		tablas.add("PRENDACONSUMOPORHABITACION");
		tablas.add("PRESTAMO");
		
		tablas.add("PRESTAMOCONSUMOPORHABITACION");
		tablas.add("PRODUCTO");
		tablas.add("PRODUCTOCONSUMOPORHABITACION");
		tablas.add("RECEPCIONISTA");
		tablas.add("RESERVA");
		tablas.add("RESERVAHABITACION");
		tablas.add("RESTAURANTE");
		tablas.add("RESTAURANTESIRVEPRODUCTO");
		tablas.add("RESTAURANTEVENDIOPRODUCTO");
		tablas.add("SALON");
		
		tablas.add("SALONCONSUMOPORHABITACION");
		tablas.add("SALONRESERVA");
		tablas.add("SERVICIOPRENDAS");
		tablas.add("SERVICIOSPA");
		tablas.add("SPA");
		tablas.add("SPACONSUMOPORHABITACION");
		tablas.add("SUPERMERCADO");
		tablas.add("SUPERMERCADOCONSUMOHABITACION");
		tablas.add("TIENDA");
		tablas.add("TIENDACONSUMOPORHABITACION");
		
		tablas.add("TIENDAVENDEPRODUCTO");
		tablas.add("TIENDAVENDIOPRODUCTO");
		tablas.add("USUARIO");
		

		
	}
	
	private PersistenciaHotelAndes(JsonObject tableConfig)
	{
		crearCalsesSQL();
		tablas = leerNombreTablas(tableConfig);
		
		String unidadPersistencia = tableConfig.get ("unidadPersistencia").getAsString ();
		log.trace("Accediendo unidad de persistencia: " + unidadPersistencia);
		pmf = JDOHelper.getPersistenceManagerFactory (unidadPersistencia);

	}
	
	public static PersistenciaHotelAndes getInstance()
	{
		if(instance == null)
		{
			instance = new PersistenciaHotelAndes();
		}
		return instance;
	}
	
	public static PersistenciaHotelAndes getInstance (JsonObject tableConfig)
	{
		if (instance == null)
		{
			instance = new PersistenciaHotelAndes (tableConfig);
		}
		return instance;
	}

	public void cerrarUnidadPersistencia ()
	{
		pmf.close ();
		instance = null;
	}
	
	private List<String> leerNombreTablas(JsonObject tableConfig)
	{
		JsonArray nombres = tableConfig.getAsJsonArray("tablas") ;

		List <String> resp = new LinkedList <String> ();
		for (JsonElement nom : nombres)
		{
			resp.add (nom.getAsString ());
		}
		
		return resp;
	}
	
	private void crearCalsesSQL()
	{
		sqlAdministradorDatos = new SQLAdministradorDatos(this);
		
		sqlBar = new SQLBar(this);
		sqlBarConsumoPorHabitacion = new SQLBarConsumoPorHabitacion(this);
		sqlBarSirveProducto = new SQLBarSirveProducto(this);
		sqlBarVendioProducto = new SQLBarVendioProducto(this);
		
		sqlCliente = new SQLCLIENTE(this);
		sqlConsumoPorHabitacion = new SQLConsumoPorHabitacion(this);
		
		sqlEmpleado = new SQLEmpleado(this);
		
		sqlFormaDePago = new SQLFormaDePago(this);
		
		sqlGerente = new SQLGerente(this);
		sqlGimnasio = new SQLGimnasio(this);
		sqlGimnasioConsumoPorHabitacion = new SQLGimnasioConsumoPorHabitacion(this);
		
		sqlHabitacion = new SQLHabitacion(this);
		sqlHotel = new SQLHotel(this);
		
		sqlInternet = new SQLInternet(this);
		sqlInternetConsumoPorHabitacion = new SQLInternetConsumoPorHabitacion(this);
		
		sqlPiscina = new SQLPiscina(this);
		sqlPlanConsumo = new SQLPlanConsumo(this);
		sqlPrestamo = new SQLPrestamo(this);
		sqlPrestamoConsumoPorHabitacion = new SQLPrestamoConsumoPorHabitacion(this);
		sqlProductoConsumoPorHabitacion = new SQLProductoConsumoPorHabitacion(this);
		
		sqlRecepcionista = new SQLRecepcionista(this);
		sqlReserva = new SQLReserva(this);
		sqlReservaHabitacion = new SQLReservaHabitacion(this); 
		sqlRestaurante = new SQLRestaurante(this);
		sqlRestauranteSirveProducto = new SQLRestauranteSirveProducto(this);
		sqlRestauranteVendioProducto = new SQLRestauranteVendioProducto(this);
		
		sqlSalon = new SQLSalon(this);
		sqlSalonConsumoPorHabitacion = new SQLSalonConsumoPorHabitacion(this);
		sqlSalonReserva = new SQLSalonReserva(this);
		sqlServicioPrendas = new SQLServicioPrendas(this);
		sqlServicioPrendasConsumoPorHabitacion = new SQLServicioPrendasConsumoPorHabitacion(this);
		sqlServicioSpa = new SQLServicioSpa(this);
		sqlSpaConsumoPorHabitacion = new SQLSpaConsumoPorHabitacion(this);
		sqlSupermercado = new SQLSupermercado(this);
		sqlSupermercadoConsumoPorHabitacion = new SQLSupermercadoConsumoPorHabitacion(this);
		
		sqlTienda = new SQLTIENDA(this);
		sqlTiendaConsumoPorHabitacion = new SQLTiendaConsumoPorHabitacion(this);
		sqlTiendaVendeProducto = new SQLTiendaVendeProducto(this);
		sqlTiendaVendioProducto = new SQLTiendaVendioProducto(this);
		sqlTipoHabitacion = new SQLTipoHabitacion(this);
		
		sqlUsuario = new SQLUsuario(this);
		
		sqlUtil = new SQLUtil(this);
	}


	�

	public List<String> getTablas() {
		return tablas;
	}

	public String getSqlAdministradorDatos() {
		return tablas.get(1);
	}

	public String getSqlBar() {
		return tablas.get(2);
	}

	public String getSqlBarConsumoPorHabitacion() {
		return tablas.get(3);
	}

	public String getSqlBarSirveProducto() {
		return tablas.get(4);
	}

	public String getSqlBarVendioProducto() {
		return tablas.get(5);
	}

	public String getSqlCliente() {
		return tablas.get(6);
	}

	public String getSqlConsumoPorHabitacion() {
		return tablas.get(7);
	}

	public String getSqlEmpleado() {
		return tablas.get(8);
	}

	public String getSqlFormaDePago() {
		return tablas.get(9);
	}

	public String getSqlGerente() {
		return tablas.get(10);
	}

	public String getSqlGimnasio() {
		return tablas.get(11);
	}

	public String getSqlGimnasioConsumoPorHabitacion() {
		return tablas.get(12);
	}

	public String getSqlHabitacion() {
		return tablas.get(13);
	}

	public String getSqlHotel() {
		return tablas.get(14);
	}

	public String getSqlInternet() {
		return tablas.get(15);
	}

	public String getSqlInternetConsumoPorHabitacion() {
		return tablas.get(16);
	}

	public String getSqlPiscina() {
		return tablas.get(17);
	}

	public String getSqlPlanConsumo() {
		return tablas.get(18);
	}

	public String getSqlPrestamo() {
		return tablas.get(19);
	}

	public String getSqlPrestamoConsumoPorHabitacion() {
		return tablas.get(20);
	}

	public String getSqlProductoConsumoPorHabitacion() {
		return tablas.get(21);
	}

	public String getSqlRecepcionista() {
		return tablas.get(22);
	}

	public String getSqlReserva() {
		return tablas.get(23);
	}

	public String getSqlReservaHabitacion() {
		return tablas.get(24);
	}

	public String getSqlRestaurante() {
		return tablas.get(25);
	}

	public String getSqlRestauranteSirveProducto() {
		return tablas.get(26);
	}

	public String getSqlRestauranteVendioProducto() {
		return tablas.get(27);
	}

	public String getSqlSalon() {
		return tablas.get(28);
	}

	public String getSqlSalonConsumoPorHabitacion() {
		return tablas.get(29);
	}

	public String getSqlSalonReserva() {
		return tablas.get(30);
	}

	public String getSqlServicioPrendas() {
		return tablas.get(31);
	}

	public String getSqlServicioPrendasConsumoPorHabitacion() {
		return tablas.get(32);
	}

	public String getSqlServicioSpa() {
		return tablas.get(33);
	}

	public String getSqlSpa() {
		return tablas.get(34);
	}

	public String getSqlSpaConsumoPorHabitacion() {
		return tablas.get(35);
	}

	public String getSqlSupermercado() {
		return tablas.get(36);
	}

	public String getSqlSupermercadoConsumoPorHabitacion() {
		return tablas.get(37);
	}

	public String getSqlTienda() {
		return tablas.get(38);
	}

	public String getSqlTiendaConsumoPorHabitacion() {
		return tablas.get(39);
	}

	public String getSqlTiendaVendeProducto() {
		return tablas.get(40);
	}

	public String getSqlTiendaVendioProducto() {
		return tablas.get(41);
	}

	public String getSqlTipoHabitacion() {
		return tablas.get(42);
	}

	public String getSqlUsuario() {
		return tablas.get(43);
	}

	public String getSqlUtil() {
		return tablas.get(44);
	}
	
	private long nextval ()
	{
        long resp = sqlUtil.nextval (pmf.getPersistenceManager());
        log.trace ("Generando secuencia: " + resp);
        return resp;
    }
	
	private String darDetalleException(Exception e) 
	{
		String resp = "";
		if (e.getClass().getName().equals("javax.jdo.JDODataStoreException"))
		{
			JDODataStoreException je = (javax.jdo.JDODataStoreException) e;
			return je.getNestedExceptions() [0].getMessage();
		}
		return resp;
	}
	
	
	
}
